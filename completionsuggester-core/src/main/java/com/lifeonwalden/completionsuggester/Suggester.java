package com.lifeonwalden.completionsuggester;

import com.lifeonwalden.completionsuggester.common.Config;
import com.lifeonwalden.completionsuggester.common.SuggesterNode;
import com.lifeonwalden.completionsuggester.common.SuggestionItemProvider;
import com.lifeonwalden.completionsuggester.support.StreamIterateProcessor;
import com.lifeonwalden.completionsuggester.support.SuggestFieldFetcher;
import com.lifeonwalden.completionsuggester.support.impl.SuggestFieldBeanFetcher;
import com.lifeonwalden.completionsuggester.support.impl.SuggestFieldMapFetcher;

import java.util.*;

public class Suggester<T> implements StreamIterateProcessor<T> {
    private Map<String, List<T>> cache = new HashMap<>();
    private Map<String, SuggesterNode<T>> matchMapping = new HashMap<>();
    private Config config;
    private SuggestFieldFetcher<T> fieldFetcher;
    private SuggestionItemProvider<T> itemProvider;

    public Suggester(SuggestionItemProvider<T> itemProvider, Config config) {
        this.config = config;
        this.itemProvider = itemProvider;

        init();
    }

    @Override
    public void process(T item) {
        if (null == this.fieldFetcher) {
            if (item instanceof Map) {
                this.fieldFetcher = new SuggestFieldMapFetcher<>();
            } else {
                this.fieldFetcher = new SuggestFieldBeanFetcher<>(item.getClass(), config.getSuggesterFields());
            }
        }

        build(item);
    }

    public void add(T item) {
        synchronized (this) {
            build(item);
        }
    }

    public Optional<T> get(String key) {
        if (null == key || key.length() == 0) {
            return Optional.empty();
        }

        SuggesterNode<T> node = this.matchMapping.get(key);
        if (null == node) {
            return Optional.empty();
        } else {
            T item = node.getItem();
            if (null == item) {
                item = this.itemProvider.fetchSpecialItem(key);
                if (null == item) {
                    this.matchMapping.remove(key);
                    return Optional.empty();
                } else {
                    node.setItem(item);
                    return Optional.of(item);
                }
            } else {
                return Optional.of(item);
            }
        }
    }

    public void remove(T item) {
        synchronized (this) {
            this.matchMapping.remove(this.config.getSuggestItemIdFetcher().fetch(item));
            this.cache.clear();
        }
    }

    public void rebuild() {
        synchronized (this) {
            this.itemProvider.fetchFullItem(this);
            this.cache.clear();
        }
    }

    public void fresh(List<String> ids) {
        List<T> items;
        if (null != ids && !ids.isEmpty()) {
            items = this.itemProvider.fetchSpecialItem(ids);
        } else {
            items = this.itemProvider.fetchLatestItem();
        }

        synchronized (this) {
            if (null != items) {
                items.forEach(item -> build(item));
            }
            this.cache.clear();
        }
    }

    public List<T> suggest(String question) {
        if (null != question && !question.isEmpty()) {
            String _question = question.toLowerCase();
            if (this.cache.containsKey(_question)) {
                return this.cache.get(_question);
            } else {
                List<String> ids = new ArrayList<>();
                int counter = 0;
                for (Map.Entry<String, SuggesterNode<T>> entry : this.matchMapping.entrySet()) {
                    for (String matchField : entry.getValue().getMatchField()) {
                        if (matchField.contains(_question)) {
                            ids.add(entry.getKey());
                            counter++;
                            break;
                        }
                    }
                    if (this.config.getMaxSuggestItemCount() <= counter) {
                        break;
                    }
                }

                List<T> itemList = this.itemProvider.fetchSpecialItem(ids);
                this.cache.put(_question, itemList);

                return itemList;
            }
        } else {
            List<String> ids = new ArrayList<>();
            int counter = 0;
            for (Map.Entry<String, SuggesterNode<T>> entry : this.matchMapping.entrySet()) {
                ids.add(entry.getKey());
                counter++;

                if (this.config.getMaxSuggestItemCount() <= counter) {
                    break;
                }
            }
            return Collections.emptyList();
        }
    }

    private void init() {
        itemProvider.fetchFullItem(this);
    }

    private void build(T item) {
        String id = this.config.getSuggestItemIdFetcher().fetch(item).trim().intern();

        List<String> itemMatchList = new ArrayList<>();
        config.getSuggesterFields().forEach(field -> {
            Optional<char[][]> _fieldAlphabet = this.config.getIndexMetaProvider().get(this.fieldFetcher.fetch(item, field).trim().toLowerCase().intern());
            if (_fieldAlphabet.isPresent()) {
                char[][] fieldAlphabet = _fieldAlphabet.get();
                for (char[] alphabet : fieldAlphabet) {
                    if (alphabet.length > 0) {
                        String matchItem = String.valueOf(alphabet).trim().toLowerCase().intern();
                        if (!matchItem.isEmpty()) {
                            itemMatchList.add(matchItem);
                        }
                    }
                }
            }
        });

        if (itemMatchList.isEmpty()) {
            this.matchMapping.remove(id);
        } else {
            this.matchMapping.put(id, new SuggesterNode<>().setMatchField(itemMatchList.toArray(new String[0])));
        }
    }
}
