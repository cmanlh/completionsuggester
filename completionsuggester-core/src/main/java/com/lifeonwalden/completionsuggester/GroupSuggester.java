package com.lifeonwalden.completionsuggester;

import com.lifeonwalden.completionsuggester.common.*;
import com.lifeonwalden.completionsuggester.support.StreamIterateProcessor;
import com.lifeonwalden.completionsuggester.support.SuggestFieldFetcher;
import com.lifeonwalden.completionsuggester.support.impl.SuggestFieldBeanFetcher;
import com.lifeonwalden.completionsuggester.support.impl.SuggestFieldMapFetcher;

import java.util.*;

public class GroupSuggester<T> implements StreamIterateProcessor<T> {
    private Map<String, Map<String, List<T>>> cache = new HashMap<>();
    private Map<String, SuggesterNode<T>> matchMapping = new HashMap<>();
    private Map<String, Set<String>> groupInfoMapping = new HashMap<>();
    private Config config;
    private SuggestFieldFetcher<T> fieldFetcher;
    private SuggestionItemProvider<T> itemProvider;
    private GroupInfoProvider groupInfoProvider;

    public GroupSuggester(SuggestionItemProvider<T> itemProvider, GroupInfoProvider groupInfoProvider, Config config) {
        this.config = config;
        this.itemProvider = itemProvider;
        this.groupInfoProvider = groupInfoProvider;

        init();
    }

    @Override
    public void process(T item) {
        if (null == this.fieldFetcher) {
            if (item instanceof Map) {
                this.fieldFetcher = new SuggestFieldMapFetcher<>();
            } else {
                this.fieldFetcher = new SuggestFieldBeanFetcher<>(item.getClass(), config.getSuggesterFields());
            }
        }

        build(item);
    }

    public void add(T item, String groupKey) {
        synchronized (this) {
            build(item);
            Set<String> groupItemSet = groupInfoMapping.get(groupKey);
            if (null == groupItemSet) {
                groupItemSet = new HashSet<>();
                groupInfoMapping.put(groupKey, groupItemSet);
            }
            groupItemSet.add(this.config.getSuggestItemIdFetcher().fetch(item));
        }
    }

    public Optional<T> get(String key) {
        if (null == key || key.length() == 0) {
            return Optional.empty();
        }

        SuggesterNode<T> node = this.matchMapping.get(key);
        if (null == node) {
            return Optional.empty();
        } else {
            T item = node.getItem();
            if (null == item) {
                item = this.itemProvider.fetchSpecialItem(key);
                if (null == item) {
                    this.matchMapping.remove(key);
                    return Optional.empty();
                } else {
                    node.setItem(item);
                    return Optional.of(item);
                }
            } else {
                return Optional.of(item);
            }
        }
    }

    public void remove(T item) {
        synchronized (this) {
            String itemKey = this.config.getSuggestItemIdFetcher().fetch(item);
            this.matchMapping.remove(itemKey);
            this.cache.clear();
            this.groupInfoMapping.values().forEach(set -> set.remove(itemKey));
        }
    }

    public void rebuild() {
        synchronized (this) {
            this.itemProvider.fetchFullItem(this);
            this.cache.clear();
            this.groupInfoMapping = this.groupInfoProvider.fetchFullGroupInfo();
            if (null == this.groupInfoMapping) {
                this.groupInfoMapping = new HashMap<>();
            }
        }
    }

    public void fresh(List<String> ids, String groupKey) {
        List<T> items;
        if (null != ids && !ids.isEmpty()) {
            items = this.itemProvider.fetchSpecialItem(ids);
        } else {
            items = this.itemProvider.fetchLatestItem();
        }
        Set<String> groupSet = null;
        if (null != groupKey) {
            groupSet = this.groupInfoProvider.fetchSpecialGroup(groupKey);
        }

        synchronized (this) {
            if (null != items) {
                items.forEach(item -> build(item));
            }
            this.cache.clear();
            if (null != groupKey && null != groupSet) {
                this.groupInfoMapping.put(groupKey, groupSet);
            } else {
                this.groupInfoMapping = this.groupInfoProvider.fetchFullGroupInfo();
                if (null == this.groupInfoMapping) {
                    this.groupInfoMapping = new HashMap<>();
                }
            }
        }
    }

    public List<T> suggest(QuestionInfo questionInfo) {
        if (null == questionInfo || null == questionInfo.getGroupKey() || questionInfo.getGroupKey().isEmpty()) {
            return Collections.emptyList();
        }
        Set<String> groupSet = groupInfoMapping.get(questionInfo.getGroupKey());
        if (null == groupSet) {
            return Collections.emptyList();
        }

        if (null != questionInfo.getQuestion() && !questionInfo.getQuestion().isEmpty()) {
            String _question = questionInfo.getQuestion().toLowerCase();
            Map<String, List<T>> groupCache = this.cache.get(questionInfo.getGroupKey());
            if (null != groupCache && groupCache.containsKey(_question)) {
                return groupCache.get(_question);
            } else {
                if (null == groupCache) {
                    groupCache = new HashMap<>();
                    this.cache.put(questionInfo.getGroupKey(), groupCache);
                }
                List<String> ids = new ArrayList<>();
                int counter = 0;
                for (Map.Entry<String, SuggesterNode<T>> entry : this.matchMapping.entrySet()) {
                    for (String matchField : entry.getValue().getMatchField()) {
                        if (matchField.contains(_question)) {
                            if (groupSet.contains(entry.getKey())) {
                                ids.add(entry.getKey());
                                counter++;
                                break;
                            }
                        }
                    }
                    if (this.config.getMaxSuggestItemCount() <= counter) {
                        break;
                    }
                }

                List<T> itemList = this.itemProvider.fetchSpecialItem(ids);
                groupCache.put(_question, itemList);

                return itemList;
            }
        } else {
            return Collections.emptyList();
        }
    }

    private void init() {
        itemProvider.fetchFullItem(this);
        groupInfoMapping = groupInfoProvider.fetchFullGroupInfo();
        if (null == this.groupInfoMapping) {
            this.groupInfoMapping = new HashMap<>();
        }
    }

    private void build(T item) {
        String id = this.config.getSuggestItemIdFetcher().fetch(item).trim().intern();

        List<String> itemMatchList = new ArrayList<>();
        config.getSuggesterFields().forEach(field -> {
            Optional<char[][]> _fieldAlphabet = this.config.getIndexMetaProvider().get(this.fieldFetcher.fetch(item, field).trim().toLowerCase().intern());
            if (_fieldAlphabet.isPresent()) {
                char[][] fieldAlphabet = _fieldAlphabet.get();
                for (char[] alphabet : fieldAlphabet) {
                    if (alphabet.length > 0) {
                        String matchItem = String.valueOf(alphabet).trim().toLowerCase().intern();
                        if (!matchItem.isEmpty()) {
                            itemMatchList.add(matchItem);
                        }
                    }
                }
            }
        });

        if (itemMatchList.isEmpty()) {
            this.matchMapping.remove(id);
        } else {
            this.matchMapping.put(id, new SuggesterNode<>().setMatchField(itemMatchList.toArray(new String[0])));
        }
    }
}
