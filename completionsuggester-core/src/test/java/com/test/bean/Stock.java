package com.test.bean;

public class Stock {
    private String code;

    private String name;

    public String getCode() {
        return code;
    }

    public Stock setCode(String code) {
        this.code = code;

        return this;
    }

    public String getName() {
        return name;
    }

    public Stock setName(String name) {
        this.name = name;

        return this;
    }
}
