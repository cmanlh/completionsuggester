package com.lifeonwalden.completionsuggester;

import com.lifeonwalden.completionsuggester.common.Config;
import com.lifeonwalden.completionsuggester.common.SuggestionItemProvider;
import com.lifeonwalden.completionsuggester.support.SuggestItemIdFetcher;
import com.lifeonwalden.completionsuggester.support.impl.IndexMetaProviderImpl;
import com.lifeonwalden.completionsuggester.support.impl.SuggestionItemProviderImpl;
import com.test.bean.Stock;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class SuggesterTest {
    private List<Stock> initOptions = new ArrayList<>();
    private List<Stock> rebuildOptions = new ArrayList<>();
    private List<Stock> freshOptions = new ArrayList<>();

    @Before
    public void init() {
        initOptions.addAll(Arrays.asList(
                new Stock().setCode("150067").setName("国泰信用互利分级债券B"),
                new Stock().setCode("150260").setName("易方达重组指数分级B"),
                new Stock().setCode("150250").setName("招商中证银行指数分级B"),
                new Stock().setCode("150165").setName("东吴中证可转换债券指数分级B"),
                new Stock().setCode("150139").setName("银华中证800等权指数增强分级B"),
                new Stock().setCode("150312").setName("信诚中证智能家居指数分级B"),
                new Stock().setCode("150139").setName("银华中证800等权指数增强分级B"),
                new Stock().setCode("150278").setName("鹏华高铁分级B"),
                new Stock().setCode("150193").setName("鹏华地产分级B"),
                new Stock().setCode("502055").setName("长盛中证证券公司分级B"),
                new Stock().setCode("150031").setName("银华中证等权90指数鑫利"),
                new Stock().setCode("150272").setName("招商国证生物医药指数分级B"),
                new Stock().setCode("166401").setName("浦银安盛稳健增利债券(LOF)C"),
                new Stock().setCode("150272").setName("招商国证生物医药指数分级B"),
                new Stock().setCode("150204").setName("鹏华传媒分级B"),
                new Stock().setCode("502025").setName("鹏华钢铁分级B"),
                new Stock().setCode("502058").setName("广发医疗指数分级B"),
                new Stock().setCode("150121").setName("银河沪深300成长优先"),
                new Stock().setCode("150315").setName("富国中证工业4.0指数分级A")
        ));

        rebuildOptions.addAll(Arrays.asList(
                new Stock().setCode("150067").setName("国泰信用互利分级债券B"),
                new Stock().setCode("150185").setName("申万菱信中证环保产业指数分级B"),
                new Stock().setCode("150250").setName("招商中证银行指数分级B"),
                new Stock().setCode("150165").setName("东吴中证可转换债券指数分级B"),
                new Stock().setCode("150316").setName("富国中证工业4.0指数分级B"),
                new Stock().setCode("150312").setName("信诚中证智能家居指数分级B"),
                new Stock().setCode("150139").setName("银华中证800等权指数增强分级B"),
                new Stock().setCode("150059").setName("银华金瑞"),
                new Stock().setCode("150193").setName("鹏华地产分级B"),
                new Stock().setCode("502055").setName("长盛中证证券公司分级B"),
                new Stock().setCode("150124").setName("建信央视财经50指数分级B"),
                new Stock().setCode("150272").setName("招商国证生物医药指数分级B"),
                new Stock().setCode("150222").setName("前海开源中航军工指数分级B"),
                new Stock().setCode("150272").setName("招商国证生物医药指数分级B"),
                new Stock().setCode("150008").setName("瑞和小康"),
                new Stock().setCode("502025").setName("鹏华钢铁分级B"),
                new Stock().setCode("164206").setName("天弘添利债券(LOF)"),
                new Stock().setCode("150121").setName("银河沪深300成长优先"),
                new Stock().setCode("150209").setName("富国中证国有企业改革指数分级A")
        ));

        freshOptions.addAll(Arrays.asList(
                new Stock().setCode("150067").setName("国泰信用互利分级债券A"),
                new Stock().setCode("160136").setName("申万菱信指数"),
                new Stock().setCode("164206").setName("天弘添利债券")
        ));
    }

    @Test
    public void setup() {
        SuggestionItemProvider provider = new SuggestionItemProviderImpl(initOptions, rebuildOptions, freshOptions);
        ((SuggestionItemProviderImpl) provider).init();
        Suggester<Stock> suggester = new Suggester<>(provider,
                new Config().setIndexMetaProvider(new IndexMetaProviderImpl())
                        .setMaxSuggestItemCount(5)
                        .setSuggesterFields(Arrays.asList("code", "name"))
                        .setSuggestItemIdFetcher((SuggestItemIdFetcher<Stock>) item -> item.getCode()));
        List<Stock> suggestion = suggester.suggest("150");
        Assert.assertTrue(5 == suggestion.size());
    }

    @Test
    public void add() {
        SuggestionItemProvider provider = new SuggestionItemProviderImpl(initOptions, rebuildOptions, freshOptions);
        ((SuggestionItemProviderImpl) provider).init();
        Suggester<Stock> suggester = new Suggester<>(provider,
                new Config().setIndexMetaProvider(new IndexMetaProviderImpl())
                        .setMaxSuggestItemCount(5)
                        .setSuggesterFields(Arrays.asList("code", "name"))
                        .setSuggestItemIdFetcher((SuggestItemIdFetcher<Stock>) item -> item.getCode()));
        Stock addedStock = new Stock().setCode("502008").setName("易方达国企改革分级B");
        ((SuggestionItemProviderImpl) provider).add(addedStock);
        suggester.add(addedStock);
        Assert.assertArrayEquals(Arrays.asList(addedStock).toArray()
                , suggester.suggest("502008").toArray());
    }

    @Test
    public void get_has() {
        SuggestionItemProvider provider = new SuggestionItemProviderImpl(initOptions, rebuildOptions, freshOptions);
        ((SuggestionItemProviderImpl) provider).init();
        Suggester<Stock> suggester = new Suggester<>(provider,
                new Config().setIndexMetaProvider(new IndexMetaProviderImpl())
                        .setMaxSuggestItemCount(5)
                        .setSuggesterFields(Arrays.asList("code", "name"))
                        .setSuggestItemIdFetcher((SuggestItemIdFetcher<Stock>) item -> item.getCode()));
        Assert.assertEquals("招商中证银行指数分级B", suggester.get("150250").get().getName());
    }

    @Test
    public void get_none() {
        SuggestionItemProvider provider = new SuggestionItemProviderImpl(initOptions, rebuildOptions, freshOptions);
        ((SuggestionItemProviderImpl) provider).init();
        Suggester<Stock> suggester = new Suggester<>(provider,
                new Config().setIndexMetaProvider(new IndexMetaProviderImpl())
                        .setMaxSuggestItemCount(5)
                        .setSuggesterFields(Arrays.asList("code", "name"))
                        .setSuggestItemIdFetcher((SuggestItemIdFetcher<Stock>) item -> item.getCode()));
        Assert.assertFalse(suggester.get("22").isPresent());
    }

    @Test
    public void add_match() {
        SuggestionItemProvider provider = new SuggestionItemProviderImpl(initOptions, rebuildOptions, freshOptions);
        ((SuggestionItemProviderImpl) provider).init();
        Suggester<Stock> suggester = new Suggester<>(provider,
                new Config().setIndexMetaProvider(new IndexMetaProviderImpl())
                        .setMaxSuggestItemCount(5)
                        .setSuggesterFields(Arrays.asList("code", "name"))
                        .setSuggestItemIdFetcher((SuggestItemIdFetcher<Stock>) item -> item.getCode()));
        Stock addedStock = new Stock().setCode("502008").setName("易方达国企改革分级B");
        ((SuggestionItemProviderImpl) provider).add(addedStock);
        suggester.add(addedStock);
        Assert.assertArrayEquals(Arrays.asList(initOptions.get(9), initOptions.get(14), initOptions.get(15), initOptions.get(16), addedStock).toArray()
                , suggester.suggest("020").toArray());
    }

    @Test
    public void remove() {
        SuggestionItemProvider provider = new SuggestionItemProviderImpl(initOptions, rebuildOptions, freshOptions);
        ((SuggestionItemProviderImpl) provider).init();
        Suggester<Stock> suggester = new Suggester<>(provider,
                new Config().setIndexMetaProvider(new IndexMetaProviderImpl())
                        .setMaxSuggestItemCount(5)
                        .setSuggesterFields(Arrays.asList("code", "name"))
                        .setSuggestItemIdFetcher((SuggestItemIdFetcher<Stock>) item -> item.getCode()));
        Stock addedStock = new Stock().setCode("150250").setName("招商中证银行指数分级B");
        suggester.remove(addedStock);
        Assert.assertArrayEquals(Arrays.asList().toArray()
                , suggester.suggest("150250").toArray());
    }

    @Test
    public void remove_match() {
        SuggestionItemProvider provider = new SuggestionItemProviderImpl(initOptions, rebuildOptions, freshOptions);
        ((SuggestionItemProviderImpl) provider).init();
        Suggester<Stock> suggester = new Suggester<>(provider,
                new Config().setIndexMetaProvider(new IndexMetaProviderImpl())
                        .setMaxSuggestItemCount(30)
                        .setSuggesterFields(Arrays.asList("code", "name"))
                        .setSuggestItemIdFetcher((SuggestItemIdFetcher<Stock>) item -> item.getCode()));
        Stock addedStock = new Stock().setCode("150250").setName("招商中证银行指数分级B");
        suggester.remove(addedStock);
        List<Stock> suggestion = suggester.suggest("150");
        Assert.assertTrue(12 == suggestion.size());
        suggestion.forEach(stock -> {
            Assert.assertTrue(stock.getCode().contains("150"));
        });
    }

    @Test
    public void match_150272() {
        SuggestionItemProvider provider = new SuggestionItemProviderImpl(initOptions, rebuildOptions, freshOptions);
        ((SuggestionItemProviderImpl) provider).init();
        Suggester<Stock> suggester = new Suggester<>(provider,
                new Config().setIndexMetaProvider(new IndexMetaProviderImpl())
                        .setMaxSuggestItemCount(5)
                        .setSuggesterFields(Arrays.asList("code", "name"))
                        .setSuggestItemIdFetcher((SuggestItemIdFetcher<Stock>) item -> item.getCode()));
        List<Stock> suggestion = suggester.suggest("150272");
        Assert.assertTrue(1 == suggestion.size());
        suggestion.forEach(stock -> {
            Assert.assertTrue(stock.getCode().contains("150272"));
        });
    }

    @Test
    public void match_15027() {
        SuggestionItemProvider provider = new SuggestionItemProviderImpl(initOptions, rebuildOptions, freshOptions);
        ((SuggestionItemProviderImpl) provider).init();
        Suggester<Stock> suggester = new Suggester<>(provider,
                new Config().setIndexMetaProvider(new IndexMetaProviderImpl())
                        .setMaxSuggestItemCount(5)
                        .setSuggesterFields(Arrays.asList("code", "name"))
                        .setSuggestItemIdFetcher((SuggestItemIdFetcher<Stock>) item -> item.getCode()));
        List<Stock> suggestion = suggester.suggest("15027");
        Assert.assertTrue(2 == suggestion.size());
        suggestion.forEach(stock -> {
            Assert.assertTrue(stock.getCode().contains("15027"));
        });
    }

    @Test
    public void match_1502() {
        SuggestionItemProvider provider = new SuggestionItemProviderImpl(initOptions, rebuildOptions, freshOptions);
        ((SuggestionItemProviderImpl) provider).init();
        Suggester<Stock> suggester = new Suggester<>(provider,
                new Config().setIndexMetaProvider(new IndexMetaProviderImpl())
                        .setMaxSuggestItemCount(30)
                        .setSuggesterFields(Arrays.asList("code", "name"))
                        .setSuggestItemIdFetcher((SuggestItemIdFetcher<Stock>) item -> item.getCode()));
        List<Stock> suggestion = suggester.suggest("1502");
        Assert.assertTrue(5 == suggestion.size());
        suggestion.forEach(stock -> {
            Assert.assertTrue(stock.getCode().contains("1502"));
        });
    }

    @Test
    public void match_150() {
        SuggestionItemProvider provider = new SuggestionItemProviderImpl(initOptions, rebuildOptions, freshOptions);
        ((SuggestionItemProviderImpl) provider).init();
        Suggester<Stock> suggester = new Suggester<>(provider,
                new Config().setIndexMetaProvider(new IndexMetaProviderImpl())
                        .setMaxSuggestItemCount(30)
                        .setSuggesterFields(Arrays.asList("code", "name"))
                        .setSuggestItemIdFetcher((SuggestItemIdFetcher<Stock>) item -> item.getCode()));
        List<Stock> suggestion = suggester.suggest("150");
        Assert.assertTrue(13 == suggestion.size());
        suggestion.forEach(stock -> {
            Assert.assertTrue(stock.getCode().contains("150"));
        });
    }

    @Test
    public void match_020() {
        SuggestionItemProvider provider = new SuggestionItemProviderImpl(initOptions, rebuildOptions, freshOptions);
        ((SuggestionItemProviderImpl) provider).init();
        Suggester<Stock> suggester = new Suggester<>(provider,
                new Config().setIndexMetaProvider(new IndexMetaProviderImpl())
                        .setMaxSuggestItemCount(5)
                        .setSuggesterFields(Arrays.asList("code", "name"))
                        .setSuggestItemIdFetcher((SuggestItemIdFetcher<Stock>) item -> item.getCode()));
        Assert.assertArrayEquals(Arrays.asList(initOptions.get(9), initOptions.get(14), initOptions.get(15), initOptions.get(16)).toArray()
                , suggester.suggest("020").toArray());
    }

    @Test
    public void match_02() {
        SuggestionItemProvider provider = new SuggestionItemProviderImpl(initOptions, rebuildOptions, freshOptions);
        ((SuggestionItemProviderImpl) provider).init();
        Suggester<Stock> suggester = new Suggester<>(provider,
                new Config().setIndexMetaProvider(new IndexMetaProviderImpl())
                        .setMaxSuggestItemCount(30)
                        .setSuggesterFields(Arrays.asList("code", "name"))
                        .setSuggestItemIdFetcher((SuggestItemIdFetcher<Stock>) item -> item.getCode()));
        List<Stock> suggestion = suggester.suggest("02");
        Assert.assertTrue(8 == suggestion.size());
        suggestion.forEach(stock -> {
            Assert.assertTrue(stock.getCode().contains("02"));
        });
    }

    @Test
    public void rebuild() {
        SuggestionItemProvider provider = new SuggestionItemProviderImpl(initOptions, rebuildOptions, freshOptions);
        ((SuggestionItemProviderImpl) provider).init();
        Suggester<Stock> suggester = new Suggester<>(provider,
                new Config().setIndexMetaProvider(new IndexMetaProviderImpl())
                        .setMaxSuggestItemCount(30)
                        .setSuggesterFields(Arrays.asList("code", "name"))
                        .setSuggestItemIdFetcher((SuggestItemIdFetcher<Stock>) item -> item.getCode()));
        ((SuggestionItemProviderImpl) provider).rebuild();
        suggester.rebuild();
        List<Stock> suggestion = suggester.suggest("150067");
        Assert.assertTrue(1 == suggestion.size());
        suggestion.forEach(stock -> {
            Assert.assertTrue(stock.getCode().contains("150067"));
        });
    }

    @Test
    public void rebuild_15006() {
        SuggestionItemProvider provider = new SuggestionItemProviderImpl(initOptions, rebuildOptions, freshOptions);
        ((SuggestionItemProviderImpl) provider).init();
        Suggester<Stock> suggester = new Suggester<>(provider,
                new Config().setIndexMetaProvider(new IndexMetaProviderImpl())
                        .setMaxSuggestItemCount(5)
                        .setSuggesterFields(Arrays.asList("code", "name"))
                        .setSuggestItemIdFetcher((SuggestItemIdFetcher<Stock>) item -> item.getCode()));
        ((SuggestionItemProviderImpl) provider).rebuild();
        suggester.rebuild();
        Assert.assertArrayEquals(Arrays.asList(rebuildOptions.get(0)).toArray()
                , suggester.suggest("15006").toArray());
    }

    @Test
    public void rebuild_1500() {
        SuggestionItemProvider provider = new SuggestionItemProviderImpl(initOptions, rebuildOptions, freshOptions);
        ((SuggestionItemProviderImpl) provider).init();
        Suggester<Stock> suggester = new Suggester<>(provider,
                new Config().setIndexMetaProvider(new IndexMetaProviderImpl())
                        .setMaxSuggestItemCount(5)
                        .setSuggesterFields(Arrays.asList("code", "name"))
                        .setSuggestItemIdFetcher((SuggestItemIdFetcher<Stock>) item -> item.getCode()));
        ((SuggestionItemProviderImpl) provider).rebuild();
        suggester.rebuild();
        Assert.assertArrayEquals(Arrays.asList(rebuildOptions.get(0), rebuildOptions.get(7), rebuildOptions.get(14)).toArray()
                , suggester.suggest("1500").toArray());
    }

    @Test
    public void rebuild_150260() {
        SuggestionItemProvider provider = new SuggestionItemProviderImpl(initOptions, rebuildOptions, freshOptions);
        ((SuggestionItemProviderImpl) provider).init();
        Suggester<Stock> suggester = new Suggester<>(provider,
                new Config().setIndexMetaProvider(new IndexMetaProviderImpl())
                        .setMaxSuggestItemCount(5)
                        .setSuggesterFields(Arrays.asList("code", "name"))
                        .setSuggestItemIdFetcher((SuggestItemIdFetcher<Stock>) item -> item.getCode()));
        ((SuggestionItemProviderImpl) provider).rebuild();
        suggester.rebuild();
        Assert.assertArrayEquals(Arrays.asList().toArray()
                , suggester.suggest("150260").toArray());
    }

    @Test
    public void rebuild_15026() {
        SuggestionItemProvider provider = new SuggestionItemProviderImpl(initOptions, rebuildOptions, freshOptions);
        ((SuggestionItemProviderImpl) provider).init();
        Suggester<Stock> suggester = new Suggester<>(provider,
                new Config().setIndexMetaProvider(new IndexMetaProviderImpl())
                        .setMaxSuggestItemCount(5)
                        .setSuggesterFields(Arrays.asList("code", "name"))
                        .setSuggestItemIdFetcher((SuggestItemIdFetcher<Stock>) item -> item.getCode()));
        ((SuggestionItemProviderImpl) provider).rebuild();
        suggester.rebuild();
        Assert.assertArrayEquals(Arrays.asList().toArray()
                , suggester.suggest("15026").toArray());
    }

    @Test
    public void rebuild_1502() {
        SuggestionItemProvider provider = new SuggestionItemProviderImpl(initOptions, rebuildOptions, freshOptions);
        ((SuggestionItemProviderImpl) provider).init();
        Suggester<Stock> suggester = new Suggester<>(provider,
                new Config().setIndexMetaProvider(new IndexMetaProviderImpl())
                        .setMaxSuggestItemCount(30)
                        .setSuggesterFields(Arrays.asList("code", "name"))
                        .setSuggestItemIdFetcher((SuggestItemIdFetcher<Stock>) item -> item.getCode()));
        ((SuggestionItemProviderImpl) provider).rebuild();
        suggester.rebuild();
        List<Stock> suggestion = suggester.suggest("1502");
        Assert.assertTrue(4 == suggestion.size());
        suggestion.forEach(stock -> {
            Assert.assertTrue(stock.getCode().contains("1502"));
        });
    }

    @Test
    public void rebuild_150() {
        SuggestionItemProvider provider = new SuggestionItemProviderImpl(initOptions, rebuildOptions, freshOptions);
        ((SuggestionItemProviderImpl) provider).init();
        Suggester<Stock> suggester = new Suggester<>(provider,
                new Config().setIndexMetaProvider(new IndexMetaProviderImpl())
                        .setMaxSuggestItemCount(30)
                        .setSuggesterFields(Arrays.asList("code", "name"))
                        .setSuggestItemIdFetcher((SuggestItemIdFetcher<Stock>) item -> item.getCode()));
        ((SuggestionItemProviderImpl) provider).rebuild();
        suggester.rebuild();
        List<Stock> suggestion = suggester.suggest("150");
        Assert.assertTrue(15 == suggestion.size());
        suggestion.forEach(stock -> {
            Assert.assertTrue(stock.getCode().contains("150"));
        });
    }

    @Test
    public void rebuild_50260() {
        SuggestionItemProvider provider = new SuggestionItemProviderImpl(initOptions, rebuildOptions, freshOptions);
        ((SuggestionItemProviderImpl) provider).init();
        Suggester<Stock> suggester = new Suggester<>(provider,
                new Config().setIndexMetaProvider(new IndexMetaProviderImpl())
                        .setMaxSuggestItemCount(5)
                        .setSuggesterFields(Arrays.asList("code", "name"))
                        .setSuggestItemIdFetcher((SuggestItemIdFetcher<Stock>) item -> item.getCode()));
        ((SuggestionItemProviderImpl) provider).rebuild();
        suggester.rebuild();
        Assert.assertArrayEquals(Arrays.asList().toArray()
                , suggester.suggest("50260").toArray());
    }

    @Test
    public void rebuild_5026() {
        SuggestionItemProvider provider = new SuggestionItemProviderImpl(initOptions, rebuildOptions, freshOptions);
        ((SuggestionItemProviderImpl) provider).init();
        Suggester<Stock> suggester = new Suggester<>(provider,
                new Config().setIndexMetaProvider(new IndexMetaProviderImpl())
                        .setMaxSuggestItemCount(5)
                        .setSuggesterFields(Arrays.asList("code", "name"))
                        .setSuggestItemIdFetcher((SuggestItemIdFetcher<Stock>) item -> item.getCode()));
        ((SuggestionItemProviderImpl) provider).rebuild();
        suggester.rebuild();
        Assert.assertArrayEquals(Arrays.asList().toArray()
                , suggester.suggest("5026").toArray());
    }

    @Test
    public void rebuild_502() {
        SuggestionItemProvider provider = new SuggestionItemProviderImpl(initOptions, rebuildOptions, freshOptions);
        ((SuggestionItemProviderImpl) provider).init();
        Suggester<Stock> suggester = new Suggester<>(provider,
                new Config().setIndexMetaProvider(new IndexMetaProviderImpl())
                        .setMaxSuggestItemCount(30)
                        .setSuggesterFields(Arrays.asList("code", "name"))
                        .setSuggestItemIdFetcher((SuggestItemIdFetcher<Stock>) item -> item.getCode()));
        ((SuggestionItemProviderImpl) provider).rebuild();
        suggester.rebuild();
        List<Stock> suggestion = suggester.suggest("502");
        Assert.assertTrue(6 == suggestion.size());
        suggestion.forEach(stock -> {
            Assert.assertTrue(stock.getCode().contains("502"));
        });
    }

    @Test
    public void match_银华中证800等权指数增强分级B() {
        SuggestionItemProvider provider = new SuggestionItemProviderImpl(initOptions, rebuildOptions, freshOptions);
        ((SuggestionItemProviderImpl) provider).init();
        Suggester<Stock> suggester = new Suggester<>(provider,
                new Config().setIndexMetaProvider(new IndexMetaProviderImpl())
                        .setMaxSuggestItemCount(5)
                        .setSuggesterFields(Arrays.asList("code", "name"))
                        .setSuggestItemIdFetcher((SuggestItemIdFetcher<Stock>) item -> item.getCode()));
        List<Stock> suggestion = suggester.suggest("银华中证800等权指数增强分级B");
        Assert.assertTrue(1 == suggestion.size());
        suggestion.forEach(stock -> {
            Assert.assertTrue(stock.getName().contains("银华中证800等权指数增强分级B") || stock.getName().contains("银华中证800等权指数增强分级b"));
        });
    }

    @Test
    public void match_中证800等权指数增强分级B() {
        SuggestionItemProvider provider = new SuggestionItemProviderImpl(initOptions, rebuildOptions, freshOptions);
        ((SuggestionItemProviderImpl) provider).init();
        Suggester<Stock> suggester = new Suggester<>(provider,
                new Config().setIndexMetaProvider(new IndexMetaProviderImpl())
                        .setMaxSuggestItemCount(5)
                        .setSuggesterFields(Arrays.asList("code", "name"))
                        .setSuggestItemIdFetcher((SuggestItemIdFetcher<Stock>) item -> item.getCode()));
        List<Stock> suggestion = suggester.suggest("中证800等权指数增强分级B");
        Assert.assertTrue(1 == suggestion.size());
        suggestion.forEach(stock -> {
            Assert.assertTrue(stock.getName().contains("中证800等权指数增强分级B") || stock.getName().contains("中证800等权指数增强分级b"));
        });
    }

    @Test
    public void match_中证() {
        SuggestionItemProvider provider = new SuggestionItemProviderImpl(initOptions, rebuildOptions, freshOptions);
        ((SuggestionItemProviderImpl) provider).init();
        Suggester<Stock> suggester = new Suggester<>(provider,
                new Config().setIndexMetaProvider(new IndexMetaProviderImpl())
                        .setMaxSuggestItemCount(30)
                        .setSuggesterFields(Arrays.asList("code", "name"))
                        .setSuggestItemIdFetcher((SuggestItemIdFetcher<Stock>) item -> item.getCode()));
        List<Stock> suggestion = suggester.suggest("中证");
        Assert.assertTrue(7 == suggestion.size());
        suggestion.forEach(stock -> {
            Assert.assertTrue(stock.getName().contains("中证"));
        });
    }

    @Test
    public void match_银华中证() {
        SuggestionItemProvider provider = new SuggestionItemProviderImpl(initOptions, rebuildOptions, freshOptions);
        ((SuggestionItemProviderImpl) provider).init();
        Suggester<Stock> suggester = new Suggester<>(provider,
                new Config().setIndexMetaProvider(new IndexMetaProviderImpl())
                        .setMaxSuggestItemCount(5)
                        .setSuggesterFields(Arrays.asList("code", "name"))
                        .setSuggestItemIdFetcher((SuggestItemIdFetcher<Stock>) item -> item.getCode()));
        List<Stock> suggestion = suggester.suggest("银华中证");
        Assert.assertTrue(2 == suggestion.size());
        suggestion.forEach(stock -> {
            Assert.assertTrue(stock.getName().contains("银华中证"));
        });
    }

    @Test
    public void fresh() {
        SuggestionItemProvider provider = new SuggestionItemProviderImpl(initOptions, rebuildOptions, freshOptions);
        ((SuggestionItemProviderImpl) provider).init();
        Suggester<Stock> suggester = new Suggester<>(provider,
                new Config().setIndexMetaProvider(new IndexMetaProviderImpl())
                        .setMaxSuggestItemCount(5)
                        .setSuggesterFields(Arrays.asList("code", "name"))
                        .setSuggestItemIdFetcher((SuggestItemIdFetcher<Stock>) item -> item.getCode()));
        ((SuggestionItemProviderImpl) provider).fresh();
        suggester.fresh(null);

        List<Stock> suggestion = suggester.suggest("150067");
        Assert.assertTrue(1 == suggestion.size());
        suggestion.forEach(stock -> {
            Assert.assertTrue(stock.getName().equals("国泰信用互利分级债券A"));
        });

        suggestion = suggester.suggest("160136");
        Assert.assertTrue(1 == suggestion.size());
        suggestion.forEach(stock -> {
            Assert.assertTrue(stock.getName().equals("申万菱信指数"));
        });

        suggestion = suggester.suggest("164206");
        Assert.assertTrue(1 == suggestion.size());
        suggestion.forEach(stock -> {
            Assert.assertTrue(stock.getName().equals("天弘添利债券"));
        });
    }
}
