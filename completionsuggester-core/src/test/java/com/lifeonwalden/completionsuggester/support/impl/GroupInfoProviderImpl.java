package com.lifeonwalden.completionsuggester.support.impl;

import com.lifeonwalden.completionsuggester.common.GroupInfoProvider;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class GroupInfoProviderImpl implements GroupInfoProvider {
    private Map<String, Set<String>> groupSets = new HashMap<>();

    @Override
    public Map<String, Set<String>> fetchFullGroupInfo() {
        Set<String> groupSet = new HashSet<>();
        groupSet.add("150067");
        groupSet.add("166401");
        groupSets.put("other", groupSet);

        groupSet = new HashSet<>();
        groupSet.add("150067");
        groupSet.add("150315");
        groupSets.put("another", groupSet);

        return groupSets;
    }

    @Override
    public Set<String> fetchSpecialGroup(String groupKey) {
        return groupSets.get(groupKey);
    }
}
