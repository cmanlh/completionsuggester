package com.lifeonwalden.completionsuggester.support.impl;

import com.lifeonwalden.completionsuggester.support.IndexMetaProvider;

import java.util.Optional;

public class IndexMetaProviderImpl implements IndexMetaProvider {
    @Override
    public Optional<char[][]> get(String words) {
        if (null == words || words.isEmpty()) {
            return Optional.empty();
        }

        return Optional.of(new char[][]{words.toCharArray()});
    }
}
