package com.lifeonwalden.completionsuggester.support.impl;

import com.lifeonwalden.completionsuggester.common.SuggestionItemProvider;
import com.lifeonwalden.completionsuggester.support.StreamIterateProcessor;
import com.test.bean.Stock;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SuggestionItemProviderImpl implements SuggestionItemProvider<Stock> {
    private List<Stock> initOptions;
    private List<Stock> rebuildOptions;
    private List<Stock> freshOptions;
    private List<Stock> items;

    public SuggestionItemProviderImpl(List<Stock> initOptions, List<Stock> rebuildOptions, List<Stock> freshOptions) {
        this.initOptions = initOptions;
        this.rebuildOptions = rebuildOptions;
        this.freshOptions = freshOptions;
    }

    public void init() {
        this.items = new ArrayList<>();
        for (Stock option : this.initOptions) {
            this.add(option);
        }
    }

    public void rebuild() {
        this.items = new ArrayList<>();
        for (Stock option : this.rebuildOptions) {
            this.add(option);
        }
    }

    public void add(Stock stock) {
        for (int i = 0; i < items.size(); i++) {
            if (this.items.get(i).getCode().equalsIgnoreCase(stock.getCode())) {
                this.items.set(i, stock);
                return;
            }
        }

        this.items.add(stock);
    }

    public void fresh() {
        this.freshOptions.forEach(item -> this.add(item));
    }

    @Override
    public void fetchFullItem(StreamIterateProcessor<Stock> processor) {
        for (Stock item : this.items) {
            processor.process(item);
        }
    }

    @Override
    public List<Stock> fetchSpecialItem(List<String> ids) {
        List<Stock> specialItemList = new ArrayList<>();
        Map<String, Boolean> toSearch = new HashMap<>();
        if (null != ids && !ids.isEmpty()) {
            for (String id : ids) {
                toSearch.put(id, null);
            }
        }

        this.items.forEach(item -> {
            if (toSearch.containsKey(item.getCode())) {
                specialItemList.add(item);
            }
        });

        return specialItemList;
    }

    @Override
    public Stock fetchSpecialItem(String id) {
        for (int i = 0; i < items.size(); i++) {
            if (this.items.get(i).getCode().equalsIgnoreCase(id)) {
                return this.items.get(i);
            }
        }

        return null;
    }

    @Override
    public List<Stock> fetchLatestItem() {
        return this.freshOptions;
    }
}
