package com.lifeonwalden.completionsuggester.common;

public class SuggesterNode<T> {
    private T item;
    private String[] matchField;

    public T getItem() {
        return item;
    }

    public SuggesterNode setItem(T item) {
        this.item = item;

        return this;
    }

    public String[] getMatchField() {
        return matchField;
    }

    public SuggesterNode setMatchField(String[] matchField) {
        this.matchField = matchField;

        return this;
    }
}
