package com.lifeonwalden.completionsuggester.common;

import com.lifeonwalden.completionsuggester.support.IndexMetaProvider;
import com.lifeonwalden.completionsuggester.support.SuggestItemIdFetcher;

import java.util.List;

public class Config {
    private IndexMetaProvider indexMetaProvider;

    private List<String> suggesterFields;

    private int maxSuggestItemCount;

    private SuggestItemIdFetcher suggestItemIdFetcher;

    public IndexMetaProvider getIndexMetaProvider() {
        return indexMetaProvider;
    }

    public Config setIndexMetaProvider(IndexMetaProvider indexMetaProvider) {
        this.indexMetaProvider = indexMetaProvider;

        return this;
    }

    public List<String> getSuggesterFields() {
        return suggesterFields;
    }

    public Config setSuggesterFields(List<String> suggesterFields) {
        this.suggesterFields = suggesterFields;

        return this;
    }

    public int getMaxSuggestItemCount() {
        return maxSuggestItemCount;
    }

    public Config setMaxSuggestItemCount(int maxSuggestItemCount) {
        this.maxSuggestItemCount = maxSuggestItemCount;

        return this;
    }

    public SuggestItemIdFetcher getSuggestItemIdFetcher() {
        return suggestItemIdFetcher;
    }

    public Config setSuggestItemIdFetcher(SuggestItemIdFetcher suggestItemIdFetcher) {
        this.suggestItemIdFetcher = suggestItemIdFetcher;

        return this;
    }
}
