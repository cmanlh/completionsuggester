package com.lifeonwalden.completionsuggester.common;

import java.util.Map;
import java.util.Set;

public interface GroupInfoProvider {
    /**
     * 处理全量组信息
     */
    Map<String, Set<String>> fetchFullGroupInfo();

    /**
     * 返回指定组的信息
     *
     * @param groupKey
     * @return
     */
    Set<String> fetchSpecialGroup(String groupKey);
}
