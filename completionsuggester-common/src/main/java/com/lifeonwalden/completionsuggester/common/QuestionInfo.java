package com.lifeonwalden.completionsuggester.common;

public class QuestionInfo {
    private String question;
    private String groupKey;

    public String getQuestion() {
        return question;
    }

    public QuestionInfo setQuestion(String question) {
        this.question = question;

        return this;
    }

    public String getGroupKey() {
        return groupKey;
    }

    public QuestionInfo setGroupKey(String groupKey) {
        this.groupKey = groupKey;

        return this;
    }
}
