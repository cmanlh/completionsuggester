package com.lifeonwalden.completionsuggester.common;

import com.lifeonwalden.completionsuggester.support.StreamIterateProcessor;

import java.util.List;

public interface SuggestionItemProvider<T> {
    /**
     * 处理全量数据项
     */
    void fetchFullItem(StreamIterateProcessor<T> processor);

    /**
     * 返回指定id数据项
     *
     * @param ids
     * @return
     */
    List<T> fetchSpecialItem(List<String> ids);

    /**
     * 返回指定id数据项
     *
     * @param id
     * @return
     */
    T fetchSpecialItem(String id);

    /**
     * 返回最新数据项，用于增量更新
     *
     * @return
     */
    List<T> fetchLatestItem();
}
