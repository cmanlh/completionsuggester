package com.lifeonwalden.completionsuggester.support;

public interface StreamIterateProcessor<T> {
    void process(T item);
}
