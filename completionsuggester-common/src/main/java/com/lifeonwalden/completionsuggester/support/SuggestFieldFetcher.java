package com.lifeonwalden.completionsuggester.support;

public interface SuggestFieldFetcher<T> {
    String fetch(T item, String field);
}