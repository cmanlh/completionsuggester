package com.lifeonwalden.completionsuggester.support.impl;

import com.lifeonwalden.completionsuggester.support.SuggestFieldFetcher;

import java.util.Map;

public class SuggestFieldMapFetcher<T> implements SuggestFieldFetcher<T> {

    @Override
    public String fetch(T item, String field) {
        return String.valueOf(((Map) item).get(field));
    }
}
