package com.lifeonwalden.completionsuggester.support;

import java.util.Optional;

public interface IndexMetaProvider {
    Optional<char[][]> get(String words);
}