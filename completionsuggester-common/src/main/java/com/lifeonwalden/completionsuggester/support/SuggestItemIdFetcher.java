package com.lifeonwalden.completionsuggester.support;

public interface SuggestItemIdFetcher<T> {
    String fetch(T item);
}
